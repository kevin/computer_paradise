var counter = 0;

function myCounter() {
    counter++;
    document.getElementById("counterDisplay").innerText = counter;
    console.log("Vous avez cliqué ", counter, "fois.");
}

document.getElementById("clickCounter").addEventListener("click", myCounter);

function changeFontSize() {
    var newSize = document.getElementById("textSize").value + "px";
    console.log("New Size: ", newSize);

    var paragraphs = document.getElementsByTagName("p");
    console.log("Paragraphes sélectionnés: ", paragraphs);
    for(var i=0; i < paragraphs.length; i++) {
        paragraphs[i].style.fontSize = newSize;
    }

   var bouton = document.getElementById("clickCounter");
   console.log("mon bouton: ", bouton.innerText);
   bouton.style.fontSize = newSize;
}

document.getElementById("textSize").addEventListener("input", changeFontSize)